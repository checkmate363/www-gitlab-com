---
layout: markdown_page
title: "Tech Stack Applications"
---

## GitLab Systems Diagram
<img src="https://docs.google.com/drawings/d/e/2PACX-1vSKA7OS_m7lzr1IzZmDcwxACFbz5rifAqMXU0lfJTd4mJhr0t60HgeyfZqEbfeSbwTUEXTbwgCFYJ2t/pub?w=1510&amp;h=766">

#### Tech Stack Support Hierarchy
1. System Owners
1. IT Operations or People Operations

System owners will remain the point of contact for provisioning and issues involving administration. In the event IT Operations or People Operations Specialists are unavailable to support issues, System Owners will be the main point of contact. IT Operations engineering empowers system owners to maintain accountability for tech systems through efforts involving automation, monitoring, and visibility. People Operations Specialists supports the tech stack listed in the [People Operations Handbook](/handbook/people-operations/).

#### [Tech Stack Applications](https://docs.google.com/spreadsheets/d/1mTNZHsK3TWzQdeFqkITKA0pHADjuurv37XMuHv12hDU/edit?usp=sharing)

<iframe src="https://docs.google.com/spreadsheets/d/e/2PACX-1vT4eyTqyEoukdelQ6O-JU_EV05a_8m1tzn8CPW73zajntShIl2V5kbmqFA3pDKw4aAlcUPHO2MyMEzZ/pubhtml?gid=0&amp;single=true&amp;widget=true&amp;headers=false"></iframe>

To complete any update/changes to the Tech Stack Applications table, please submit an [issue request](https://gitlab.com/gitlab-com/business-ops/Business-Operations/issues/new?issuable_template=Update%20Tech%20Stack%20Information) with the requested changes and @ mention 'gitlab-com/business-ops/bizops-bs'.
Also see "Operations License Tracking & Contract Details" which can be found on the Google Drive.

#### Requesting Access to Tech Stack applications

Access to the applications listed in our [Tech Stack Applications](https://docs.google.com/spreadsheets/d/1mTNZHsK3TWzQdeFqkITKA0pHADjuurv37XMuHv12hDU/edit?usp=sharing) table should be requested by submitting an [Access Request](https://gitlab.com/gitlab-com/access-requests/issues/new?issuable_template=New%20Access%20Request). Please note (with the exception of Bulk Access Request) that you will need to complete **one** issue per Team Member. **It is the responsibility of the requestor to determine who the System Provisioner is and assign to impacted provisioner(s).**

#### Asking for Help with Salesforce, Outreach, or other tools in our Tech Stack

Questions about usage, routing, or integration concerns for applications should be submitted as an issue using the following template in [_issues.help](
https://gitlab.com/gitlab-com/business-ops/Issues.help/issues/new?issuable_template=Question_Salesforce_Outreach&issue%5Btitle%5D=Tool_Question:%20Write%20Here&issue%5Bdescription%5D)

#### Integrating Other tools

The tech stack is the approved GitLab tech stack. No one is permitted to integrate any other tool into the tech stack without asking Operations for permission as it affects our system security and database integrity.

### Export Control Classification, and Countries We Do Not Do Business In

GitLab's Export Control Classification (or ECCN) is 5D992.c. As a consequence of this classification, we currently do not do business in: Iran, Sudan (excluding South Sudan), Syria, North Korea, and Cuba.